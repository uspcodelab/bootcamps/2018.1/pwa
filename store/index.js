import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: {
      isLoggedIn: false,
      email: "",
      token: "",
      // variable to know the action of the login page
      isRegister: false
    },
    mutations: {
      login(state, { token }) {
        state.isLoggedIn = true
        state.email = "alex@oleao.com"
        state.token = "11111"
      },
      logout(state) {
        state.isLoggedIn = false
        state.email = ""
        state.token = ""
      }
    },
    actions: {
      async signin ({ commit }) {
        let token = "2222"
        commit('login', { token })
      },
      async signout ({ commit }) {
        commit('logout')
      }
    }
  })
}

export default createStore
